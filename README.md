# Toolbox

Toolbox is a tool for Linux operating systems, which allows the use of containerized command line environments. It is built on top of Podman and other standard container technologies from OCI.

## Installation
Toolbox can be installed on Fedora Workstation (or any package-based version of Fedora) with the following command:

```
sudo dnf install toolbox
```

Toolbox is preinstalled on Fedora Silverblue 30 or newer. On older versions, it can be installed with the following command:
```
rpm-ostree install toolbox
```

## Step by step guides

### Set up machine learning environment
THE GOAL: Environment with python, pytorch, conda and Kdevelop (IDE)
Let´s assume toolbox is already installed.

1) Create a toolbox environment
```
toolbox create machineLearning
```

2) Enter environment
```
toolbox enter machineLearning
```

3) If toolbox environment does not have python3 already installed:
```
sudo dnf install python3
```

4) download anaconda
```
sudo dnf install conda
```

5) init conda
```
conda init
```

6) Exit toolbox (need to reopen terminal for conda activation) -- Ctrl+D

7) Enter toolbox
```
toolbox enter machineLearning
```

8) Create new conda environment:
```
conda create -n machine_learning
```

9) Enter conda environment:
```
conda activate machine_learning
```

10) Install pytorch:
```
conda install pytorch
```

11) Install Kdevelop:
```
sudo dnf install kdevelop
```

12) Test if everything is installed correctly:
a) create python file:
```
touch test.py
```
b) Run Kdevelop (from toolbox env, where pytorch is installed from previous steps)
```
kdevelop
```
c) Open test.py with Kdevelop IDE and insert following lines of code:
```
import torch

a = torch.Tensor([2, 6])
b = torch.Tensor([3, 2])

print(a*b)
```
d) In Kdevelop (click: run, Configure launches, change settings to: Run script application)
e) Outup should looks like this:
```
tensor([ 6., 12.])
```
If It does. Well. Congratulation! You set up environment for machine learning with working IDE :)

## Troubleshooting

### Container is still running
If user exit the container, It should stop working, but It will not. 
Let's see, what happends if we exit the container and look at current containers by following command:
```
toolbox list -c
```
 
Output looks like this:
```   
CONTAINER ID  CONTAINER NAME  CREATED       STATUS   IMAGE NAME
4affecac07a2  cuda            26 hours ago  exited   registry.fedoraproject.org/fedora-toolbox:35
9bae5d370d3a  keras           2 days ago    running  registry.fedoraproject.org/fedora-toolbox:35
f74554bf46f9  testone         3 days ago    exited   registry.fedoraproject.org/fedora-toolbox:35
24867d36334e  torch           2 hours ago   exited   registry.fedoraproject.org/fedora-toolbox:35
```

So, we are not in container and It is still running. Toolbox is still not well document and there is no official way to stop it.
But following command do it for us. (Since toolbox is running on top of podman, we can use it)
```    
podman stop <container_name>
```

In our case:
```
podman stop keras
```
Output after listing containers:
```
CONTAINER ID  CONTAINER NAME  CREATED       STATUS  IMAGE NAME
4affecac07a2  cuda            26 hours ago  exited  registry.fedoraproject.org/fedora-toolbox:35
9bae5d370d3a  keras           2 days ago    exited  registry.fedoraproject.org/fedora-toolbox:35
f74554bf46f9  testone         3 days ago    exited  registry.fedoraproject.org/fedora-toolbox:35
24867d36334e  torch           2 hours ago   exited  registry.fedoraproject.org/fedora-toolbox:35	
```

